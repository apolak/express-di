import express = require('express');
import { Application, Request, Response, NextFunction } from "express";
import UsersService from "./src/users-service";
import DbService from "./src/db-service";
import { asClass, AwilixContainer, createContainer, InjectionMode, Lifetime } from "awilix";

interface UsersRequest extends Request {
    usersService: UsersService
}

const { scopePerRequest, inject } = require('awilix-express');

const container: AwilixContainer = createContainer({
    injectionMode: InjectionMode.CLASSIC
});

container.register({
    usersService: asClass(UsersService).classic(),
    dbService: asClass(DbService)
});

const app: Application = express();

app.use(scopePerRequest(container));

app.get('/users', inject('usersService'), (req: UsersRequest, res: Response, next: NextFunction) => {
   res.send(
       req.usersService.getUsers()
   );
});

app.listen(3000);