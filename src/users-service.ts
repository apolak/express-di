import DbService from "./db-service";

export default class UsersService {
    constructor(private dbService: DbService) {
    }

    getUsers(): any {
        return this.dbService.fire();
    }
}